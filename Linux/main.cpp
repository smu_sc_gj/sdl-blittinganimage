/*
  Based on http://lazyfoo.net/SDL_tutorials/
  Used with permission. 
*/

//Include SDL library
//This is now cross platform. 
#if defined(_WIN32)
    #include "SDL.h"
#else
    #include "SDL/SDL.h"
#endif

int main( int argc, char* args[] )
{
    //Declare pointers to surface structures
    SDL_Surface* hello = NULL;
    SDL_Surface* screen = NULL;

    //Initialise SDL
    //NOTE: SDL_INIT_EVERYTHING is a constant defined by SDL
    SDL_Init( SDL_INIT_EVERYTHING );

    //Surface structure we'll use for the window - screen
    //This is essentially the back buffer. 
    screen = SDL_SetVideoMode( 640, 480, 32, SDL_SWSURFACE );

    //Load our bitmap image
    hello = SDL_LoadBMP( "beefy-miracle.bmp" );

    //Blit the image to the screen
    //Draw to backbuffer
    SDL_BlitSurface( hello, NULL, screen, NULL );

    //Refresh the screen (replace with backbuffer. 
    SDL_Flip( screen );

    //Pause to allow the image to be seen
    SDL_Delay( 10000 );

    //Free the loaded bitmap
    SDL_FreeSurface( hello );

    //Shutdown SDL - clear up resorces etc. 
    SDL_Quit();

    return 0;
}
